var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

/* GET home page. */
router.get('/recommendArticleList', function(req, res, next) {
  const Article = mongoose.model('Article');
  Article.find({}, {}, {limit: 10}, (err, articleList) => {
    if(err){
      res.send(false);
    } else {
      res.send(articleList);
    }
  })
});
router.get('/recommendAuthorList', function(req, res, next) {
  const Author = mongoose.model('Author');
  Author.find({}, {}, {limit: 4}, (err, authorList) => {
    if(err){
      res.send(false);
    } else {
      res.send(authorList);
    }
  })
});

router.get('/follow', function(req, res, next) {
  res.send(true);
});

router.get('/article', function(req, res, next) {
  const Article = mongoose.model('Article');
  const query = req.query;
  const id = query.id;
  Article.findById(id, (err, articleList) => {
    if(err){
      res.send(false);
    } else {
      res.send(articleList);
    }
  })
});
router.post('/article/add', async function(req, res, next) {
  const Article = mongoose.model('Article');
  let data = req.body;
  const article = new Article(data);
  const model = await article.save();
  res.send(model);
});
router.post('/author/add', async function(req, res, next) {
  const Author = mongoose.model('Author');
  let data = req.body;
  const author = new Author(data);
  const model = await author.save();
  res.send(model);
});

module.exports = router;
