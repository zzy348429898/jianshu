var mongoose = require('mongoose');

var AuthorSchema = new mongoose.Schema({
    id: Number,
    name: String,
    letterCount: Number,
    likeCount: Number,
    imgUrl: String,
});

mongoose.model('Author', AuthorSchema);