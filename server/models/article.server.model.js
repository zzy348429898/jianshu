var mongoose = require('mongoose');

var ArticleSchema = new mongoose.Schema({
    title: String,
    content: String,
    abstract: String,
    authorName: String,
    authorLevel: Number,
    recommendList: Array,
    ceateTime: String,
    readCount: Number,
    likeCount: Number,
    letterCount: Number,
    imgUrl: String,
});

mongoose.model('Article', ArticleSchema);