# jianshu

## 3rd part library

umi.js: init the web project;

express.js: a node server;

## how to preview

you can preview this site on http://8.134.211.160:8200/

## source code

https://gitlab.com/zzy348429898/jianshu

## how to run locally

```shell

git clone git@gitlab.com:zzy348429898/jianshu.git

cd ./web

npm install

npm run start

cd ./server

npm install

npm run start

```
