import style from './index.less';

interface Props {
    code: string
}
const Iconfont = (props: Props) => {
    return (
        <span
            className={style.font}
            dangerouslySetInnerHTML={{__html: props.code}}
        >
            {/* {props.code} */}
        </span>
    )
}

export default Iconfont;