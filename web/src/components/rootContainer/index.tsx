import React from 'react';
import style from './index.less';
interface Props {
    children: React.ReactElement
}
const RootContainer = (props: Props) => {
    return (
        <>
            {props.children}
        </>
    )
}

export default RootContainer