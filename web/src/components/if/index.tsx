interface Props {
    condition: boolean,
    children: React.ReactElement
}

const If = (props: Props) => {
    return props.condition ? props.children : null
};

export default If