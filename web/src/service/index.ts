import React from 'react';
import request from '@/utils/request';
import { AxiosPromise, AxiosResponse } from 'axios';
import { Article, RecommendArticle, RecommendAuthor } from './vo';
export const getRecommendList: () => AxiosPromise<RecommendArticle[]> = () => {
    return request({
        method: 'get',
        url: '/recommendArticleList'
    })
}

export const getRecommendAuthorList: () => AxiosPromise<RecommendAuthor[]> = () => {
    return request({
        method: 'get',
        url: '/recommendAuthorList'
    })
}
export const follow: () => AxiosPromise<boolean> = () => {
    return request({
        method: 'get',
        url: '/recommendAuthorList'
    })
}
export const getArticle: (params: {id: string}) => AxiosPromise<Article> = (params) => {
    return request({
        method: 'get',
        url: '/article',
        params
    })
}