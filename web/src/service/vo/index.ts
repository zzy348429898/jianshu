export interface RecommendArticle {
    _id: number,
    title: string,
    abstract: string,
    authorName: string,
    readCount: number,
    commentCount: number,
    likeCount: number,
    rewardCount: number,
    imgUrl?: string
}

export interface RecommendAuthor {
    _id: number,
    name: string,
    letterCount: 124566,
    likeCount: 44654,
    imgUrl: string,
}

export interface Article {
    _id: number,
    title: string,
    content: string,
    authorName: string,
    ceateTime: string,
    readCount: number,
    likeCount: number,
    letterCount: number,
    recommendList: Article[]
    imgUrl: string,
    authorLevel: number,
}