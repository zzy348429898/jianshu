import React from 'react';
import style from './index.less';
import {message} from 'antd';
import { getArticle, getRecommendList } from '@/service';
import { Article, RecommendArticle } from '@/service/vo';
import { useEffect } from 'react';
import { useState } from 'react';
import If from '@/components/if';
import { useMemo } from 'react';
import ArticleInfo from './components/authorInfo';
import Iconfont from '@/components/iconfont';
import { useLocation } from 'umi';
interface State {
    article?: Article,
    recommendData?: RecommendArticle[],
}
const initState = {

}

const Detail = () => {
    const location: any = useLocation();
    const id = location.query.id
    const [state, setState] = useState<State>(initState);
    const  init = async () => {
        try {
            const articleResp = await getArticle({id});
            setState(state => ({...state, article: articleResp.data}));
            const recommendArticle = await getRecommendList();
            setState(state => ({...state, recommendData: recommendArticle.data}));
        } catch (error) {
            message.error('初始化失败！');
        }
    };
    useEffect(() => {
        init();
    }, []);

    const recommendAuthorArticleList = useMemo(() => {
        if(state.article){
            return state.article?.recommendList || [];
        } else {
            return []
        }
    }, [state.article]);
    return (
        <div className={style.root}>
            <div className={style.container}>
                <div className={style.flexCol}>
                    <div className={style.left}>
                        <If condition={Boolean(state.article)}>
                            <div className={style.article}>
                                <div className={style.header}>{state.article?.title}</div>
                                <div className={style.authorInfo}>
                                    <ArticleInfo article={state.article}></ArticleInfo>
                                </div>
                                <div
                                    className={style.content}
                                    dangerouslySetInnerHTML={{__html: state.article?.content}}></div>
                            </div>
                        </If>
                    </div>
                    <div className={style.right}>
                        <div className={style.card}>
                            {
                                recommendAuthorArticleList.map(item => (
                                    <>
                                        <div className={style.titleRow}>{item.title}</div>
                                        <div className={style.readCountRow}>{`阅读 ${item.readCount}`}</div>
                                    </>
                                ))
                            }
                        </div>
                        <div className={style.card}>
                            <div className={style.recommendTitle}>推荐阅读</div>
                            <If condition={Boolean(state.recommendData)}>
                                <div className={style.card}>
                                    {
                                        state.recommendData?.map(item => (
                                            <>
                                                <div className={style.titleRow}>{item.title}</div>
                                                <div className={style.readCountRow}>{`阅读 ${item.readCount}`}</div>
                                            </>
                                        ))
                                    }
                                </div>
                            </If>
                        </div>
                    </div>
                </div>
            </div>
            <div className={style.leftBtnList}>
                <div className={style.item}>
                    <div className={style.circle} style={{fontSize: '26px'}}>
                        <Iconfont code='&#xe607;'></Iconfont>
                    </div>
                    <div className={style.letter}>{state.article?.likeCount}赞</div>
                </div>
                <div className={style.item}>
                    <div className={style.circle}>
                        赏
                    </div>
                    <div className={style.letter}>赞赏</div>
                </div>
                <div className={style.item}>
                    <div className={style.circle}>
                        <Iconfont code='&#xe608;'></Iconfont>
                    </div>
                    <div className={style.letter}>更多好文</div>
                </div>
            </div>
        </div>
    )
}

export default Detail