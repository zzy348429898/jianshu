import Iconfont from "@/components/iconfont";
import { Article, Recommendarticle as IRecommendarticle } from "@/service/vo";
import { useMemo } from "react";
import style from './index.less';

interface Props {
    article: Article
};

const ArticleInfo = (props: Props) => {
    const letterCount = useMemo(() => {
        const res = (props.article.letterCount / 1000).toFixed(1)
        return res;
    }, [props.article.letterCount]);
    const likeCount = useMemo(() => {
        const res = (props.article.likeCount / 1000).toFixed(1)
        return res;
    }, [props.article.likeCount]);
    return (
        <div className={style.articleItem}>
            <div className={style.left}>
                <img className={style.img} src={props.article.imgUrl}></img>
            </div>
            <div className={style.mid}>
                <div className={style.nameRow}>
                    <span className={style.name}>{props.article.authorName}</span>
                    <span className={style.logo}></span>
                    <button className={style.follow}>关注</button>
                </div>
                <span className={style.detail}>
                    <span className={[style.item, style.red].join(' ')}>
                        <span className={style.key}>
                            <Iconfont code="&#xe602;"></Iconfont>
                        </span>
                        <span className={style.value}>{props.article.authorLevel}</span>
                    </span>
                    <span className={style.item}>{props.article.ceateTime}</span>
                    <span className={style.item}>
                        <span className={style.key}>字数</span>
                        <span className={style.value}>{props.article.letterCount}</span>
                    </span>
                    <span className={style.item}>
                        <span className={style.key}>阅读</span>
                        <span className={style.value}>{props.article.readCount}</span>
                    </span>
                </span>
            </div>
        </div>
    )
};

export default ArticleInfo;