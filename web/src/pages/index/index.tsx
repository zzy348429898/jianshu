import React, { useEffect, useMemo, useState} from 'react';
import style from './index.less';
import {message} from 'antd';
import { getRecommendAuthorList, getRecommendList } from '@/service';
import { RecommendArticle as IRecommendArticle, RecommendAuthor as IRecommendAuthor } from '@/service/vo';
import RecommendArticle from './components/recommendArticle';
import RecommendAuthor from './components/recommendAuthor';
import {v4 as uuid} from 'uuid';
import Iconfont from '@/components/iconfont';
interface State {
  recommendData: IRecommendArticle[],
  recommendAuthorData: IRecommendAuthor[]
}
const initState: State = {
  recommendData: [],
  recommendAuthorData: []
};
export default function IndexPage() {
  const [state, setState] = useState<State>(initState);
  const init = async () => {
    try {
      const articleResp = await getRecommendList();
      const articleList = articleResp.data;
      setState(state => ({...state, recommendData: articleList}));
      const authorResp = await getRecommendAuthorList();
      const authorList = authorResp.data;
      setState(state => ({...state, recommendAuthorData: authorList}));
    } catch (error) {
      message.error('初始化失败！');
    }
  };
  const articleList = useMemo(() => {
    const data = state.recommendData;
    const res = data.map(article => {
      return <RecommendArticle article={article} key={article._id}></RecommendArticle>
    });
    return res;
  }, [state.recommendData]);
  const authorList = useMemo(() => {
    const data = state.recommendAuthorData;
    const res = data.map(author => {
      return <RecommendAuthor author={author} key={author._id}></RecommendAuthor>
    });
    return res;
  }, [state.recommendAuthorData]);
  useEffect(() => {
    init();
  }, []);
  return (
    <div className={style.root}>
      <div className={style.flexCol}>
        <div className={style.left}>
          {articleList}
        </div>
        <div className={style.right}>
          <div className={style.header}>
            <span>推荐作者</span>
            <span className={style.nextBatch}>
              <Iconfont code={'&#xe609; '}></Iconfont>
              换一批
            </span>
          </div>
          {authorList}
        </div>
      </div>
    </div>
  );
}
