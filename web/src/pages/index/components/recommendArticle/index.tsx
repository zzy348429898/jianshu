import Iconfont from '@/components/iconfont';
import If from '@/components/if';
import { RecommendArticle as IRecommendArticle } from '@/service/vo';
import style from './index.less';
import {history} from 'umi';
interface Props {
    article: IRecommendArticle
}

const RecommendArticle = (props: Props) => {
    const toDetail = (id) => {
        history.push({
            pathname: '/detail',
            query: {
                id: `${props.article._id}`
            }
        })
    };
    return (
        <div className={style.item}>
            <div className={style.content}>
                <a className={style.title} onClick={toDetail}>{props.article.title}</a>
                <p className={style.abstract}>{props.article.abstract}</p>
                <div className={style.meta}>
                    <span className={style.read}>
                        <Iconfont code='&#xe602; '></Iconfont>
                        <span className={style.num}>{props.article.readCount}</span>
                    </span>
                    <span className={style.hover} onClick={toDetail}>
                        {props.article.authorName}
                    </span>
                    <span className={style.hover} onClick={toDetail}>
                        <Iconfont code='&#xe605; '></Iconfont>
                        <span className={style.num}>{props.article.commentCount}</span>
                    </span>
                    <span className={style.name}>
                        <Iconfont code='&#xe603; '></Iconfont>
                        <span className={style.num}>{props.article.likeCount}</span>
                    </span>
                    <span className={style.name}>
                        <Iconfont code='&#xe606; '></Iconfont>
                        <span className={style.num}>{props.article.rewardCount}</span>
                    </span>
                </div>
            </div>
            <If condition={Boolean(props.article.imgUrl)}>
                <div className={style.img}>
                    <img src={props.article.imgUrl} onClick={toDetail}></img>
                </div>
            </If>
        </div>
    );
}

export default RecommendArticle;