import Iconfont from "@/components/iconfont";
import { RecommendAuthor as IRecommendAuthor } from "@/service/vo";
import { useMemo } from "react";
import style from './index.less';

interface Props {
    author: IRecommendAuthor
};

const RecommendAuthor = (props: Props) => {
    const letterCount = useMemo(() => {
        const res = (props.author.letterCount / 1000).toFixed(1)
        return res;
    }, [props.author.letterCount]);
    const likeCount = useMemo(() => {
        const res = (props.author.likeCount / 1000).toFixed(1)
        return res;
    }, [props.author.likeCount]);
    return (
        <div className={style.authorItem}>
            <div className={style.left}>
                <img className={style.img} src={props.author.imgUrl}></img>
            </div>
            <div className={style.mid}>
                <a className={style.name}>{props.author.name}</a>
                <span className={style.detail}>写了{letterCount}k字 · {likeCount}k喜欢</span>
            </div>
            <div className={style.right}>
                <span className={style.follow}>
                    <Iconfont code={'&#xe604; '}></Iconfont>
                    关注
                </span>
            </div>
        </div>
    )
};

export default RecommendAuthor;