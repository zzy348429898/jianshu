import React from 'react';
import RootContainer from './components/rootContainer';
export function rootContainer(container) {
    return (
        <RootContainer>
            {container}
        </RootContainer>
    )
  }